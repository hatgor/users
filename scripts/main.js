let users = {};
let buttonForStart = document.getElementById('createButton');
let nameFromInput = document.getElementById('userName');


function init() {
    let wrapperForCards = document.getElementById('usersBlock');
    users['user1'] = new User('user1');
    users['user1'].setUserName('Johny Cash');
    users['user1'].card.render(wrapperForCards);

    users['user2'] = new User('user2');
    users['user2'].setUserName('Steve Jobs');
    users['user2'].card.render(wrapperForCards);

    users['user3'] = new User('user3');
    users['user3'].setUserName('Bob Hope');
    users['user3'].card.render(wrapperForCards);
}

function User(userId) {
    let userName = '';
    let wrapper = document.createElement('div');

    let name = document.createElement('h5');
    name.classList.add('card-title');

    function clearAllEventListeners(element) {
        let newElement = element.cloneNode(true);
        element.parentNode.replaceChild(newElement, element);
        return newElement
    }

    function removeUser() {
        let renameButton = document.getElementById('renameButton');
        let renameButtonNew = clearAllEventListeners(renameButton);
        nameFromInput.value = '';
        buttonForStart.style.display = 'inline';
        renameButtonNew.style.display = 'none';
        wrapper.style.display = 'none';
        delete users[userId];
    }
    
    function renameUser() {
        let renameButton = document.getElementById('renameButton');
        let renameButtonNew = clearAllEventListeners(renameButton);
        nameFromInput.value = userName;
        buttonForStart.style.display = 'none';
        renameButtonNew.style.display = 'inline';
        renameButtonNew.addEventListener('click', function (event) {
            event.preventDefault();
            userName = nameFromInput.value;
            name.innerText = nameFromInput.value;
            nameFromInput.value = '';
            buttonForStart.style.display = 'inline';
            renameButtonNew.style.display = 'none';
        }, {
            once: true,
        })
    }
    
    

    function Card() {
        wrapper.setAttribute('id', userId);
        wrapper.classList.add('card');

        let cardBody = document.createElement('div');
        cardBody.classList.add('card-body');

        let renameButton = document.createElement('button');
        renameButton.innerText = 'Rename';
        renameButton.classList.add('renameBtn');
        renameButton.classList.add('btn');
        renameButton.classList.add('btn-primary');

        let removeButton = document.createElement('button');
        removeButton.innerText = 'Remove';
        removeButton.classList.add('btn');
        removeButton.classList.add('btn-primary');
        
        renameButton.addEventListener('click', renameUser);
        
        removeButton.addEventListener('click', removeUser);

        this.render = function (elem) {
            elem.appendChild(wrapper);
            wrapper.appendChild(cardBody);
            name.innerText = userName;
            cardBody.appendChild(name);
            cardBody.appendChild(renameButton);
            cardBody.appendChild(removeButton);

        }
    }

    this.card = new Card();

    this.setUserName = function (username) {
        userName = username;
    };
    this.getUserName = function () {
        return userName;
    }
}

buttonForStart.addEventListener('click', function (event) {
    event.preventDefault();
    let date = new Date();
    let hash = date.getTime();
    let userId = 'user' + hash;
    
    let wrapperForCards = document.getElementById('usersBlock');

    users[userId] = new User(userId);
    users[userId].setUserName(nameFromInput.value);
    users[userId].card.render(wrapperForCards);

    nameFromInput.value = '';
});